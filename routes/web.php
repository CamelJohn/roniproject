<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'UserChartController@index')->name('home')->middleware('auth');


Route::group(['middleware' => 'auth'], function () {
		Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
		Route::get('try', ['as' => 'pages.try', 'uses' => 'PageController@try']);
		
		Route::get('subjectIndex', ['as' => 'pages.subjectIndex', 'uses' => 'SubjectController@subjectIndex']);
		Route::get('createSubject/{id}', ['as' => 'createSubject', 'uses' => 'SubjectController@create']);
		Route::post('storeSubject', ['as' => 'storeSubject', 'uses' => 'SubjectController@store']);
		Route::get('editsubject/{id}', ['as' => 'editsubject', 'uses' => 'SubjectController@edit']);
		Route::get('subjectStatus/{id}', ['as' => 'subjectStatus', 'uses' => 'SubjectController@subjectStatus']);
		Route::put('updateSubject/{id}', ['as' => 'updateSubject', 'uses' => 'SubjectController@updateSubject']);
		Route::DELETE('deleteSubject/{id}', ['as' => 'deleteSubject', 'uses' => 'SubjectController@destroy']);

		Route::get('meetingsIndex', ['as' => 'meetingsIndex', 'uses' => 'MeetingController@index']);
		Route::get('create', ['as' => 'createMeeting', 'uses' => 'MeetingController@create']);
		Route::put('update/{id}', ['as' => 'updateMeeting', 'uses' => 'MeetingController@update']);
		Route::post('store', ['as' => 'storeMeeting', 'uses' => 'MeetingController@store']);
		Route::get('edit/{id}', ['as' => 'editMeeting', 'uses' => 'MeetingController@editMeeting']);
		Route::get('show/{id}', ['as' => 'showMeeting', 'uses' => 'MeetingController@show']);
		Route::DELETE('delete/{id}', ['as' => 'deleteMeeting', 'uses' => 'MeetingController@destroy']);
		Route::get('trashedMeeting', ['as' => 'trashedMeeting', 'uses' => 'MeetingController@trashed']);
		Route::put('restoreMeeting/{id}', ['as' => 'restoreMeeting', 'uses' => 'MeetingController@restore']);
		Route::get('attended/{id}/{meetingId}', ['as' => 'attended', 'uses' => 'MeetingController@attended']);
		Route::get('closeMeeting/{meetingId}', ['as' => 'closeMeeting', 'uses' => 'MeetingController@closeMeeting']);
		
		Route::get('tasksIndex', ['as' => 'tasksIndex', 'uses' => 'TaskController@index']);
		Route::get('createTask/{id}', ['as' => 'createTask', 'uses' => 'TaskController@create']);
		Route::get('editTask/{id}', ['as' => 'editTask', 'uses' => 'TaskController@edit']);
		Route::get('showTask/{id}', ['as' => 'showTask', 'uses' => 'TaskController@show']);
		Route::post('storeTask/{id}', ['as' => 'storeTask', 'uses' => 'TaskController@store']);
		Route::put('updateTask/{task_id}', ['as' => 'updateTask', 'uses' => 'TaskController@update']);
		Route::get('taskStatus/{task_id}', ['as' => 'taskStatus', 'uses' => 'TaskController@taskStatus']);
		Route::DELETE('deleteTask/{id}', ['as' => 'deleteTask', 'uses' => 'TaskController@destroy']);
		Route::get('trashedTask', ['as' => 'trashedTask', 'uses' => 'TaskController@trashed']);
		Route::put('restore/{id}', ['as' => 'restore', 'uses' => 'TaskController@restore']);

		Route::get('homeMail', ['as' => 'mail', 'uses' => 'MailController@home']);
		Route::post('send/email', 'MailController@sendemail');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show','create','store']]);
	Route::get('invite', ['as' => 'invite', 'uses' => 'UserController@invite']);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::get('try', ['as' => 'pages.try', 'uses' => 'UserChartController@index']);
});

Route::get('userAdd', ['as' => 'addUser', 'uses' => 'UserController@create']);
Route::post('userStore', ['as' => 'userStore', 'uses' => 'UserController@store']);



