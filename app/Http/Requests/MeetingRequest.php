<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meeting_title' => 'required|alpha_dash|min:5',
            'meeting_start' => 'required',
            'meeting_end' => 'required|after:meeting_start',
            'meeting_date' => 'required|date|after:today',
            'users' => 'required|min:1'
        ];
    }
}
