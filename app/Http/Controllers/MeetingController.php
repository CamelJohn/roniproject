<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Meeting;
use App\Subject;
use App\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Charts\UserChart;
use App\Http\Requests\MeetingRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $inviter = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $inviters = $inviter->pluck('id');
    
        if($atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager'){
            $query = DB::table('meeting_user')->whereIn('user_id',$inviters)->pluck('meeting_id');
        }
        elseif($atuh_user->role == 'vp_coordinator'){
            $query = DB::table('meetings')->where('user_id',$atuh_user->id)->pluck('id');
        }
        else{
            $query = DB::table('meeting_user')->whereIn('user_id',$atuh_user)->pluck('meeting_id');
        }
        $meetings = Meeting::whereIn('id',$query)->get();
        $farsh = false;
        $filteredmeeting = $meetings->whereIn('user_id',$inviter->values()->pluck('id'));
        $user = $atuh_user;

        return view('pages.meetingsIndex')
                ->with('meetings', $filteredmeeting)
                ->with('inviter', $inviter)
                ->with('atuh_user',$atuh_user)
                ->with('farsh', $farsh)
                ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('employee')){
            abort(403,"");
        }
        
        $id =Auth::id();
        $atuh_user = User::findOrFail($id);
        $users = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();

        return view('pages.createMeeting')->with('users',$users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeetingRequest $request)
    {
        $id = Auth::id();
        
        $meeting = new Meeting();
        $meeting->user_id = $id;
        $meeting->meeting_title=$request->meeting_title;
        $meeting->meeting_date=$request->meeting_date;
        $meeting->meeting_start=$request->meeting_start;
        $meeting->meeting_end=$request->meeting_end;
        $meeting->save();
        
        $subjects = Subject::where('meeting_id', $meeting->id)->count();
        
        if($request->users)
        {
            $meeting->users()->attach($request->users);
        }

        $user = Auth::id();
        $user = User::findOrFail($user);
        
        session()->flash('success', 'meeting created successfully');
        
        return view('pages.createSubject')
        ->with('meeting',$meeting)
        ->with('numOfSubjects', $subjects)
        ->with('user',$user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $connected = Auth::id();
        $meeting = Meeting::findOrFail($id);
        $users = User::all();
        $user = User::findOrFail($meeting->user_id);
        $tasks = Task::all();
        $tasksfilter = $tasks->whereIn('meeting_id',$meeting);

        $lists = Meeting::findOrFail($id)->users()->get();
        $lists1 = Meeting::findOrFail($id)->tasks()->get();
     
        $subjects = Meeting::findOrFail($id)->subjects()->get();

        $start = Carbon::parse($meeting->meeting_start);
        $end = Carbon::parse($meeting->meeting_end);
        $length = $end->diffInMinutes($start);
        $status = DB::table('meeting_user')->selectRaw('meeting_id = ?',[$meeting->id])->get();
        
        $attendance = $meeting->users()->where('attended','=',1)->get();
        
        return view('pages.showMeeting')
        ->with('meeting', $meeting)
        ->with('user', $user)
        ->with('lists', $lists)
        ->with('subjects',$subjects)
        ->with('users',$users)
        ->with('length', $length)
        ->with('connected', $connected)
        ->with('tasks',$tasksfilter)
        ->with('status',$status)
        ->with('attendance', $attendance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editMeeting($id)
    {
        if(Gate::allows('employee')){
            abort(403,"");
        }
        $uId =Auth::id();
        $atuh_user = User::findOrFail($uId);
        $users = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $meeting = Meeting::find($id);
        
        $subjects = Subject::all()->where('meeting_id','=', $meeting->id);

        return view('pages.createMeeting')
        ->with('meeting', $meeting)
        ->with('users',$users)
        ->with('subjects',$subjects);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, MeetingRequest $request)
    {
        $meetings = Meeting::find($id);
        $meetings->meeting_title = $request->meeting_title;
        $meetings->meeting_start = $request->meeting_start;
        $meetings->meeting_end = $request->meeting_end;
        $meetings->update();
    
        for($i=1;;$i++){
            if($request->$i != null){
                $subject = Subject::FindOrFail($request->id);
                $subject->meeting_id=$meetings->id;
                $subject->title=$request->$i;
                $subject->subject_start=$request->$i;
                $subject->save();
            }
            else{
                break;
            }
        }
        if($request->users)
        {
            $meetings->users()->sync($request->users);
        }
        $user = Auth::id();
        $user = User::findOrFail($user);
        return redirect('meetingsIndex')->with('user', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('employee')){
            abort(403,"");
        }
        $meeting = Meeting::withTrashed()->where('id',$id)->first();
        
        if($meeting->trashed()){
            $meeting->forceDelete();
        }
        else{
            $meeting->delete();
        }
        $user = Auth::id();
        $user = User::findOrFail($user);
        return redirect('meetingsIndex')->with('user', $user);
    }

    public function trashed()
    {
        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $inviter = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $trashed = Meeting::onlyTrashed()->get();
       
        $filteredtrashed = $trashed->whereIn('user_id',$inviter->values()->pluck('id'));
        
        $inviter = User::All();
        $farsh = true;

        $user = $atuh_user;
         
        return view('pages.meetingsIndex')
                ->with('meetings', $trashed)
                ->with('inviter', $inviter)
                ->with('atuh_user',$atuh_user)
                ->with('user',$user)
                ->with('farsh', $farsh);
    }

    public function restore($id)
    {
        $meeting = Meeting::withTrashed()->where('id',$id)->restore();
        return redirect()->back();
    }

    
    public function attended($id, $meetingId)
    {
        $meeting = Meeting::findOrFail($meetingId);
        
        if ($meeting->meeting_start < now() && $meeting->meeting_date < today()){
        $meeting->users()->updateExistingPivot($id, ['attended' => 1]);

        session()->flash('success', 'Attendace !');
        }
        else{
            session()->flash('error', 'the meeting has not begun !');
        }

        return redirect()->back()->with('meeting',Meeting::findOrFail($meetingId));
    }

    public function closeMeeting($meetingId)
    {
        $meeting = Meeting::findOrFail($meetingId);

        if ($meeting->meeting_start < now() && $meeting->meeting_date < today()){
            $meeting->meeting_status = 1;
            $meeting->save();

            session()->flash('success', 'Meeting Ajurned !');
        }

        else{

            session()->flash('error', '808 | the meeting has not started yet');

                return redirect()->back();
        }
        return redirect()->back();
    }
}
