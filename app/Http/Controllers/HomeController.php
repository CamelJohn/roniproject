<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Charts\UserChart;
use App\User;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        $flag = 1;
        $hashed = Hash::make('12345678');
        if (Hash::check($request->password, $hashed)) {
            return view('auth.changePassword')->with('flag', $flag);
        }
        $user = Auth::id();
        $user = User::findOrFail($user);
        
        return view('dashboard')->with('user',$user);
    }
}
