<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Meeting;
use App\Task;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TaskRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $users = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $meetings = Meeting::all();
        $filteredmeeting = $meetings->whereIn('user_id',$users->values()->pluck('id'));
        $tasks = Task::all();
        if($atuh_user->role == 'employee'){
        $filteredtask = $tasks->whereIn('task_ordered',$atuh_user->id);
        }
        else
        {
            $filteredtask = $tasks->whereIn('user_id',$users->values()->pluck('id'));
        }

        $farsh = true;
        
        $user = $atuh_user;

        return view('pages.tasksIndex')
            ->with('tasks', $filteredtask)
            ->with('users',$users) 
            ->with('meetings',$filteredmeeting)
            ->with('user',$user)
            ->with('farsh',$farsh)
            ->with('atuh_user',$atuh_user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        if(Gate::allows('employee')){
            abort(403,"");
        }

        $meeting = Meeting::findOrFail($id);
        $meeting_users=$meeting->users()->get();
        // dd($meeting_users); 
        return view('pages.createTask')
        ->with('meeting',$meeting)
        ->with('meeting_users',$meeting_users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request,$meeting)
    {
        $id = Auth::id();
        $user = User::find($id);

        $task = new Task();
        $task->user_id = $user->id;
        $task->title = $request->title;
        $task->task_start = $request->task_start;
        $task->task_ordered = $request->users;
        $task->task_end = $request->task_end;
        
        $months =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Nov','Dec'];

        $month = Carbon::createFromFormat('Y-m-d H:i:s',now())->month;

        if($month == 1)
            {$monthNum = $months[0];}
        elseif ($month == 2)
        {$monthNum = $months[1];}
        elseif ($month == 3)
        {$monthNum = $months[2];}
        elseif ($month == 4)
        {$monthNum = $months[3];}
        elseif ($month == 5)
        {$monthNum = $months[4];}
        elseif ($month == 3)
        {$monthNum = $months[5];}
        elseif ($month == 7)
        {$monthNum = $months[6];}
        elseif ($month == 8)
        {$monthNum = $months[7];}
        elseif ($month == 9)
        {$monthNum = $months[8];}
        elseif ($month == 10)
        {$monthNum = $months[9];}
        elseif ($month == 11)
        {$monthNum = $months[10];}
        elseif ($month == 12)
        {$monthNum = $months[11];}
        
        $task->monthInt = $month;
        $task->monthStr = $monthNum;
        $task->meeting_id = $meeting;
        $task->save();


        return redirect('tasksIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasks = Task::findOrFail($id);
        return view('pages.showTask')->with('tasks',$tasks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if(Gate::allows('employee')){
            abort(403,"");
        }
        $users = User::all();
        $meetings = Task::find($id)->meeting()->get();
        $tasks = Task::find($id);
        
        $ordered = Task::find($id)->user()->get('name');

        return view('pages.createTask')
        ->with('tasks',$tasks)
        ->with('users',$users)
        ->with('meetings',$meetings)
        ->with('ordered', $ordered);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request, $id)
    {   
        $task = Task::find($id);
        $task->title = $request->title;
        $task->meeting_id = $request->meetings;
        $task->task_ordered = $request->users;
        $task->task_start = $request->task_start;
        $task->task_ordered = $request->users;
        $task->task_end = $request->task_end;
        
        $task->save();

        return redirect('tasksIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('employee')){
            abort(403,"");
        }
        $task = Task::withTrashed()->where('id',$id)->first();
        
        if($task->trashed()){
            $task->forceDelete();
        }
        else{
            $task->delete();
        }
        
        return redirect('tasksIndex');
    }

    public function assignTask($id){

        $user = User::findOrFail($id);


        return view('pages.assignTask')->with('user',$user);
    }

    public function trashed(){

        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $users = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $meetings = Meeting::all();
        $filteredmeeting = $meetings->whereIn('user_id',$users->values()->pluck('id'));
        $trashed = Task::onlyTrashed()->get();
        $filteredtrashed = $trashed->whereIn('user_id',$users->values()->pluck('id'));
        
        $user = $atuh_user;

        $farsh = false;

        return view('pages.tasksIndex')
        ->with('tasks',$filteredtrashed)
        ->with('users',$users)
        ->with('user',$user)
        ->with('farsh',$farsh)
        ->with('atuh_user',$atuh_user)
        ->with('meetings',$filteredmeeting);
    }

    public function restore($id){
        $task = Task::withTrashed()->where('id',$id)->restore();
        return redirect()->back();
    }

    public function taskStatus($id){

        $tasks = Task::findOrFail($id);
        $tasks->status = ! $tasks->status;
        
        if ($tasks->task_end < today()){
            $tasks->message = 'Late again !?';
            $tasks->lateness = 1;
        }
        else{
            $tasks->lateness = 0;
        }

        $tasks->ended_at = today();
        $start = Carbon::parse($tasks->task_start);
        $end = Carbon::parse($tasks->ended_at);
        $diff = $end->diffInMinutes($start);
        
        $tasks->diff_minutes = $diff;

        $tasks->save();

        return redirect()->back();

    }

}