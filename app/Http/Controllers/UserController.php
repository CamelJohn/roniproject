<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $roles = Role::all();
        $model = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        return view('users.index', ['users' => $model])->with('roles', $roles);
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $users = User::all()->whereIn('role',['CEO', 'executive_manager']);
        return view('users.create')->with('users',$users);
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $model = new User();
        $model->name=$request->name;
        $model->email=$request->email;
        $model->phone=$request->phone;
        $model->organization_id=$request->organization;
        $model->role = 'employee';
        $model->password=Hash::make($request->get('password'));
        
        $model->save();
        return redirect()->route('login')->withStatus(__('User successfully created.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {   
       
        $roles = Role::all();
        $auth = Auth::id();
        $current = User::findOrFail($auth);

        if ($user->id == 1) {
            return redirect()->route('user.index');
        }

        return view('users.edit')->with('user', $user)->with('roles', $roles)->with('current', $current);
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User  $user)
    {
        $user->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$request->get('password') ? '' : 'password'])
            );
        $user->role = $request->roles;
        $user->update();

        return redirect()->route('user.index')->withStatus(__('User successfully updated.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        if ($user->id == 1) {
            return abort(403);
        }

        $user->delete();

        return redirect()->route('user.index')->withStatus(__('User successfully deleted.'));
    }

    public function invite(){
        $id = Auth::id();
        $user = User::findOrFail($id)->firstOrFail();
        // dd($user->email);
        return view('users.invite')->with('user', $user);

    }

}
