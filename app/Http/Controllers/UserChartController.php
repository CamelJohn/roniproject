<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Charts\UserChart;
use App\Charts\kpi;
use Illuminate\Http\Request;
use App\User;
use App\Meeting;
use App\Task;
use Illuminate\Support\Facades\Auth;


class UserChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::id();
        $user = User::find($auth);
        $userOrg = $user->organization_id;
        $users = User::all()->where('organization_id',$userOrg)->pluck('id');
        $usersCount = $users->count();
        $allOrgUsers= User::whereIn('id',$users)->get();
        $allOrgUsersCount= User::whereIn('id',$users)->count();

//מספר עובדים לפי תפקיד
        $executive_managerUsersCount =$allOrgUsers->where('role','executive_manager')->count();
        $executive_names =$allOrgUsers->where('role','executive_manager')->pluck('name');
        $vp_coordinatorUsersCount =$allOrgUsers->where('role','vp_coordinator')->count();
        $vp_names =$allOrgUsers->where('role','vp_coordinator')->pluck('name');
        $employeeUsersCount = $allOrgUsers->where('role','employee')->count();
        $employeeNames = $allOrgUsers->where('role','employee')->pluck('name');
        
        $orgMeeting = Meeting::whereIn('user_id',$users)->get();
        $orgMeetingCount = $orgMeeting->count();
 
//פגישות ארגון מטבלת הקשר, עבור נתונים כמו נוכחות וכאלה
        $meetingPivot = DB::table('meeting_user')
                    //  ->select('attended')
                     ->whereIn('meeting_id', $orgMeeting->pluck('id'))
                     ->get();
//כמות נוכחים בפגישות לכלל הארגון
        $attended = $meetingPivot->where('attended',1)->count();
        // $numOfNot =  $attended->where('attended',0)->count();
        // dd($numOfPresent);
//אחוז הנוכחים בפגישות לכלל הארגון
        if($meetingPivot->count() == 0)
            {$present = 0;}
        else
        {$present = ($attended/$meetingPivot->count());}
        // dd($auth);
//כמות הפגישות למי שמזמין פגישה        
        $meetingForOrganizer = $orgMeeting->where('user_id', $auth);
        $countMeetingOrganizer = $meetingForOrganizer->count();
//מספר הנוכחים בפגישות של המזמין פגישה
        $attendedInOrganizerMeeting = $meetingPivot->whereIn('meeting_id',$meetingForOrganizer->pluck('id'));
//אחוז נוכחות בפגישות אלו
        if($attendedInOrganizerMeeting->count() == 0)
        {$OrganizerMeetingPresent = 0;}
        else{$OrganizerMeetingPresent = ($attendedInOrganizerMeeting->where('attended',1)->count()/$attendedInOrganizerMeeting->count());}
        
        
//פגישות עבור עובד
        $meetingsForUser = $meetingPivot->where('user_id',$auth);
//כמות פעמים שנכח בפגישות
        $userAttended = $meetingsForUser->where('attended',1)->count();
//אחוז הנוכחות של העובד בפגישות אליהן הוזמן
        if($meetingsForUser->count() == 0)
        {$userPresent = 0;}
        else
        {$userPresent = ($userAttended/$meetingsForUser->count());}

        $tasks = Task::all();
        if($user->role == 'employee')
        {
                $orgTasks = $tasks->whereIn('task_ordered', $user->id);
        }
        if($user->role == 'CEO' || $user->role == 'executive_manager' )
        {
                $orgTasks = $tasks->whereIn('task_ordered', $users);
        }
        if($user->role == 'vp_coordinator' )
        {
                $orgTasks = $tasks->whereIn('user_id', $user->id);
        }

        $countTasks = $orgTasks->count();
        $doneTasks = $orgTasks->where('status', 1);
        $taskTimes = $orgTasks->where('status', 1)->pluck('diff_minutes')->avg();
    
        $doneTasksCount = $doneTasks->count();
        $avg = ($countTasks/$usersCount);
        $lateness = $orgTasks->where('lateness', 1)->count();

        if($doneTasksCount == 0)
        {$latePrecent = 0;}
        else
        {$latePrecent = ($lateness/$doneTasksCount)*100;}
        

        $sum_diff = $orgTasks->pluck('diff_minutes')->sum();
        if($countTasks == 0)
        {$avg_diff = 0;}
        else
        {$avg_diff = $sum_diff/$countTasks;}
        
        
 //סינון משימות לפי עובד, בלי קשר לדרגה שלו
        $taskForEmployee = $orgTasks->where('task_ordered',$auth);
        $countEmployeeTask = $taskForEmployee->count();
        $doneTasksForEmployee = $taskForEmployee->where('status', 1)->count();

        if($countTasks == 0){
            $doneTasksCount = 0;
            $late = 0;
        }
        else{
            $doneTasksCount = ($doneTasksCount/$countTasks)*100;
            $late = ($lateness/$countTasks)*100;
        }
        
        $label = DB::table('users')->pluck('name')->toArray();
        $data = DB::table('users')->pluck('id')->toArray();
        $label1 = DB::table('users')->where('role','!=', 'CEO')->pluck('name')->toArray();
        $count = DB::table('users')->count();
        $data1 = DB::table('tasks')->where('status','=',0)->pluck('user_id')->toArray();

        $ddf =[$executive_managerUsersCount,$vp_coordinatorUsersCount,$employeeUsersCount];
        $ddflabels =['executive_manager','vp_coordinator','employee'];

        $usersChart = new kpi;
        $usersChart->barwidth(0.9);
        $usersChart->displaylegend(false);
        $usersChart->labels($ddflabels);
        $usersChart->dataset('', 'doughnut', $ddf)
            ->color('black')
            ->backgroundColor(['dodgerblue','purple','yellowgreen'])
            ->fill(false)
            ->linetension(0.3);

        $linedata = DB::table('tasks')->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id,monthStr'))->groupBy('task_ordered','monthStr')->pluck('id');
        $linedata1 = DB::table('tasks')->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id,monthStr'))->where('lateness',1)->groupBy('task_ordered','monthStr')->pluck('id');
        $linelabels = DB::table('tasks')->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id,monthStr'))->groupBy('task_ordered','monthStr')->pluck('monthStr');
            
        $usersChart1 = new UserChart;
        $usersChart1->barwidth(0.5);
        $usersChart1->displaylegend(false);
        $usersChart1->labels($linelabels);
        $usersChart1->dataset('', 'line', $linedata)
            ->color('#00d6b4')
            ->backgroundColor('rgba(66,134,121,0.15)')
            ->fill(true)
            ->linetension(0.3);  
        $usersChart1->dataset('', 'line', $linedata1)
            ->color('red')
            ->backgroundColor('rgba(255,0,0,0.15)')
            ->fill(true)
            ->linetension(0.3);  
        
        $text = $count;
        $title = ['display' => false, 'text' => number_format($text,0), 'position' => 'bottom'];
        
        $kpilabel = ['done','assigned'];
        // $kpi1data = $orgTasks->where('status','=',1)->pluck('status');
        $taskId = $orgTasks->pluck('id');
        $taskId1 = DB::table('tasks')->whereIn('id',$taskId)->get(['id','status','lateness']);
        //Monster querry !!!!!
        $kpi1data = DB::table('tasks')->whereIn('id',$taskId)->select(DB::raw('count(status) as status'))->where('status','=',1)->groupBy('status')->pluck('status');
        $kpi4data = DB::table('tasks')->whereIn('id',$taskId)->select(DB::raw('count(status) as status'))->where('status','=',1)->where('lateness','=',0)->groupBy('status')->pluck('status');
        $kpi2data = DB::table('tasks')->whereIn('id',$taskId)->select(DB::raw('count(status) as status'))->where('status','=',0)->groupBy('status')->pluck('status');
        $kpi3data = DB::table('tasks')->whereIn('id',$taskId)->select(DB::raw('count(lateness) as lateness'))->where('lateness','=',1)->groupBy('lateness')->pluck('lateness');
        $df = [$kpi1data,$kpi2data];
        
        $usersChart2 = new kpi;
        $usersChart2->barwidth(0.0);
        $usersChart2->displaylegend(false);
        $usersChart2->labels($kpilabel);
        $usersChart2->title('', 13, 'white',false,'Arial');
        $title2 = ['display' => true, 'text' => number_format($doneTasksCount,2).' %' , 'position' => 'bottom'];
        $usersChart2->options(['rotation' => 1*pi(), 'circumference' => 1*pi(),'title'=>$title2]);
        $usersChart2->dataset('', 'doughnut', $df)
            ->color("black")
            ->backgroundColor(['limegreen','white'])
            ->fill(false)
            ->linetension(0.7);
        // $latePrecent
        $df5 =[$kpi3data,$kpi4data];
        $kpilabel5 = ['late','done'];

        $usersChart5 = new kpi;
        $usersChart5->barwidth(0.0);
        $usersChart5->displaylegend(false);
        $usersChart5->labels($kpilabel5);
        $usersChart5->title('', 13, 'white',false,'Arial');
        // $title = ['display' => true, 'text' => $kpilabel, 'position' => 'bottom'];
        $title5 = ['display' => true, 'text' => number_format($latePrecent,2).' %' , 'position' => 'bottom'];
        $usersChart5->options(['rotation' => 1*pi(), 'circumference' => 1*pi(),'title'=>$title5]);
        $usersChart5->dataset('', 'doughnut', $df5)
            ->color("black")
            ->backgroundColor(['orange','white'])
            ->fill(false)
            ->linetension(0.7);
            
            if($user->role == 'CEO' ||$user->role == 'executive_manager')
            {
                $bardata = DB::table('tasks')->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')->pluck('id');
                $bardata1 = DB::table('tasks')->whereIn('task_ordered',$users)->where('lateness',1)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')->pluck('id');
                $bardata2 = DB::table('tasks')->whereIn('task_ordered',$users)->where('status',1)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')->pluck('id');
                $barlabels = DB::table('tasks')->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')
                ->join('users','users.id','=','task_ordered')->select('users.name')->groupBy('users.name')->pluck('users.name');          
            }
            else if($user->role == 'vp_coordinator')
            {
                $bardata = DB::table('tasks')->where('user_id',$user->id)->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')->pluck('id');
                $bardata1 = DB::table('tasks')->where('user_id',$user->id)->whereIn('task_ordered',$users)->where('lateness',1)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')->pluck('id');
                $bardata2 = DB::table('tasks')->where('user_id',$user->id)->whereIn('task_ordered',$users)->where('status',1)->select(DB::raw('count(id) as id'))->groupBy('user_id')->pluck('id');
                $barlabels = DB::table('tasks')->where('user_id',$user->id)->whereIn('task_ordered',$users)->select(DB::raw('count(id) as id'))->groupBy('task_ordered')
                ->join('users','users.id','=','task_ordered')->select('users.name')->groupBy('users.name')->pluck('users.name'); 
            }

        if($user->role != 'employee'){
                $title = ['display' => true, 'text' => number_format($text,0), 'position' => 'bottom'];
                $usersChart3 = new UserChart;
                $usersChart3->barwidth(0.9);
                // $usersChart3->title('poop', 18, 'white',false,'arial');
                $usersChart3->displaylegend(false);
                $usersChart3->labels($barlabels);
                $usersChart3->dataset('', 'bar', $bardata)
                ->color("dodgerblue")
                ->backgroundColor('rgba(29,140,248,0.2)')
                ->fill(false)
                ->linetension(0.3);
                $usersChart3->dataset('', 'bar', $bardata2)
                        ->color("#00d6b4")
                        ->backgroundColor('rgba(66,134,121,0.15)')
                        ->fill(false)
                        ->linetension(0.3);
                $usersChart3->dataset('', 'bar', $bardata1)
                ->color("red")
                ->backgroundColor('rgba(255,0,0,0.15)')
                ->fill(false)
                ->linetension(0.3);

                return view('pages.try')
                ->with('avg',$avg)
                ->with('taskTimes',$taskTimes)
                ->with('user',$user)
                ->with('countTasks',$countTasks)
                ->with('countMeetingOrganizer',$countMeetingOrganizer)
                ->with('orgMeetingCount',$orgMeetingCount)
                ->with('late',$late)
                ->with('doneTasksCount',$doneTasksCount)
                ->with('usersChart', $usersChart)
                ->with('usersChart1', $usersChart1)
                ->with('usersChart2', $usersChart2)
                ->with('usersChart3', $usersChart3)
                ->with('usersChart5', $usersChart5)
                ->with('attended', $attended)
                ->with('allOrgUsersCount', $allOrgUsersCount);
                }
        else{
                return view('pages.try')
                ->with('avg',$avg)
                ->with('taskTimes',$taskTimes)
                ->with('user',$user)
                ->with('countTasks',$countTasks)
                ->with('late',$late)
                ->with('doneTasksCount',$doneTasksCount)
                ->with('attended',$attended)
                ->with('usersChart', $usersChart)
                ->with('usersChart1', $usersChart1)
                ->with('usersChart2', $usersChart2)
                ->with('usersChart5', $usersChart5)
                ->with('allOrgUsersCount', $allOrgUsersCount);
                }
        }
    }
