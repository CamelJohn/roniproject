<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    public function home(){

        return view('mail.home');

    }

    public function sendemail(Request $get){
        $email = $get->email;
        $subject = $get->subject;
        $message = $get->message;
        
        $validator = Validator::make($get->all(), [
            'email' => 'required|email',
            'subject' => 'required',
            ]);
            
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator)
                ->withInput();
            }

        Mail::to($email)->send( new SendMail($subject, $message));

        session()->flash('success', 'the e-mail was sent successfully');
        return redirect()->back();
    }
}
