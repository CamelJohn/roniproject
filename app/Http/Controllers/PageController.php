<?php

namespace App\Http\Controllers;

use App\User;
use App\Meeting;
use App\Subject;
use App\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Charts\UserChart;
use App\Http\Requests\MeetingRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;

class PageController extends Controller
{
    /**
     * Display tables page
     *
     * @return \Illuminate\View\View
     */
    public function tables()
    {
        return view('pages.tables');
    }
    public function try()
    {
        return view('pages.try');
    }
    

    public function subjectIndex()
    {
        $subjects = Subject::all();

        return view('pages.subjectIndex')->with('subjects', $subjects);
    }
    
}
