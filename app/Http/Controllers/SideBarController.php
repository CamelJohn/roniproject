<?php

namespace App\Http\Controllers;

use App\User;
use App\Meeting;
use App\Subject;
use App\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Charts\UserChart;
use App\Http\Requests\MeetingRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;

class SideBarController extends Controller
{
    public function filterMeetings()
    {
   
        $id = Auth::id();
        $user = User::FindOrFail($id);

        return view('pages.filterMeetings')->with('user', $user);
        
    }
    public function filterTasks()
    {

    }
}
