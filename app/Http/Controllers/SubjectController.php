<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Meeting;
use App\Subject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SubjectController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($meeting_id)
    {
        $meeting = Meeting::findOrFail($meeting_id);
        $numOfSubjects = Subject::where('meeting_id', $meeting_id)->count();
        return view('pages.createSubject')
        ->with('meeting',$meeting)
        ->with('numOfSubjects', $numOfSubjects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $numOfSubjects = Subject::where('meeting_id', $request->meeting_id)->count();
        $meeting = Meeting::findOrFail($request->meeting_id);
        $validator = Validator::make($request->all(), [
            'title' => 'required|alpha_dash|min:5',
            'subject_start' => 'required|after:meeting_start|before:meeting_end',
        ]);
            
        if ($validator->fails()) {
            return view('pages.createSubject')
                        ->with('meeting',$meeting)
                        ->with('numOfSubjects', $numOfSubjects)
                        ->withErrors($validator);
        }
        $subject = new Subject();
        $subject->meeting_id=$request->meeting_id;
        $subject->title=$request->title;
        $subject->subject_start=$request->subject_start;
        $subject->save();

        $user = Auth::id();
        $user = User::findOrFail($user);
       
        return view('pages.createSubject')
                ->with('meeting',$meeting)
                ->with('user',$user)
                ->with('numOfSubjects', $numOfSubjects);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::find($id);
        $meetings = Meeting::all();

        return view('pages.editsubject')->with('subject',$subject)->with('meetings', $meetings);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSubject(Request $request, $id)
    {
        $subjects = Subject::find($id);
        $farsh = false;
        $validator = Validator::make($request->all(), [
            'title' => 'required|alpha_dash|min:5',
            'subject_start' => 'required',
            ]);
            
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator)
                ->withInput();
            }
            $subjects->meeting_id = $request->meetings;
            $subjects->title = $request->title;
            $subjects->subject_start = $request->subject_start;
            $subjects->update();
            
        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $inviter = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $meetings = Meeting::All();
       
        $filteredmeeting = $meetings->whereIn('user_id',$inviter->values()->pluck('id'));
 
        return view('pages.meetingsIndex')
                ->with('meetings', $filteredmeeting)
                ->with('inviter', $inviter)
                ->with('atuh_user',$atuh_user)
                ->with('farsh', $farsh);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();
        
        $id = Auth::id();
        $atuh_user = User::findOrFail($id);
        $inviter = $atuh_user->where('organization_id',$atuh_user->organization_id)->get();
        $meetings = Meeting::All();
        $farsh = false;
        $filteredmeeting = $meetings->whereIn('user_id',$inviter->values()->pluck('id'));
        
        return view('pages.meetingsIndex')
                ->with('meetings', $filteredmeeting)
                ->with('inviter', $inviter)
                ->with('atuh_user',$atuh_user)
                ->with('farsh', $farsh);
    }

    public function subjectStatus($id)
    {   
        $subjects = Subject::findOrFail($id);
        $meeting = Meeting::findOrFail($subjects->meeting_id);

        if ($meeting->meeting_start < now() && $meeting->meeting_date < today()){ 
            if($subjects->subject_start < now()){

                $subjects->status = ! $subjects->status;
         
                $subjects->save();
             
                session()->flash('success', 'subject status updated !');
            }
            else{
                session()->flash('error', 'it\'s not time for this subject yet !');
                }
            }
        else{

            session()->flash('error', 'the meeting has not begun yet !');
            }
            
        return redirect()->back();
    }
}
