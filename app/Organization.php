<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable =['organization_name'];

    public function users(){
        return $this->hasMany('App\User');

    }
}
