<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('CEO', function ($user) {
            return $user->role == 'CEO';
        });

        Gate::define('executive_manager', function ($user) {
            return $user->role == 'executive_manager';
        });

        Gate::define('vp_coordinator', function ($user) {
            return $user->role == 'vp_coordinator';
        });

        Gate::define('employee', function ($user) {
            return $user->role == 'employee';
        });
    }
}
