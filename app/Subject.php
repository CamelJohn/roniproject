<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{    //
    protected $fillable = [
        'meeting_id', 'title', 'status', 'subject_start'
    ];

   


    public function meeting(){
        return $this->belongsTo('App\Meeting');

    }
}
