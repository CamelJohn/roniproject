<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;
    
    protected $fillable =[
        'meeting_id', 'title', 'task_ordered', 'user_id','task_start', 'task_end','status',
    ];


    public function user(){
        return $this->belongsTo('App\User');

    }

    public function meeting(){
        return $this->belongsTo('App\Meeting');

    }
}
