<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meeting extends Model
{
    use SoftDeletes;
    
    protected $fillable =['id','user_id', 'meeting_start', 'meeting_end','meeting_title']; 

    public function users(){
        return $this->belongsToMany('App\User')->withPivot('attended');

    }

    public function subjects(){
        return $this->hasMany('App\Subject');

    }

    public function tasks(){
        return $this->hasMany('App\Task');
    }
}
