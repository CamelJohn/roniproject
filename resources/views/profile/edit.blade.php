@extends('layouts.app', ['page' => __('User Profile'), 'pageSlug' => 'profile'])

@section('content')
    <div class="row" style="font-family: 'Lobster', cursive;">
        
            <div class="card col-md-5 ml-5 my-5">
                <div class="card-header">
                    <h5 class="title pb-3 pt-3" style="border-bottom:solid 1px white">Edit Profile</h5>
                </div>
                <form method="post" action="{{ route('profile.update') }}" autocomplete="off">
                    <div class="card-body col-md-10 offset-1">
                            @csrf
                            @method('put')

                            @include('alerts.success')

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label class="mb-3" style="color:white; font-weight:bold">Name</label>
                                <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name', auth()->user()->name) }}">
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label class="mb-3" style="color:white; font-weight:bold">Email address</label>
                                <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" value="{{ old('email', auth()->user()->email) }}">
                                @include('alerts.feedback', ['field' => 'email'])
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                <label class="mb-3" style="color:white; font-weight:bold">Phone Number</label>
                                <input type="text" name="phone" cols="5" rows="5" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="{{ auth()->user()->phone }}" value="{{ old('phone', auth()->user()->phone) }}">
                                @include('alerts.feedback', ['field' => 'phone'])
                            </div>
                            @if($user->role == 'CEO')
                            <div class="form-group{{ $errors->has('role') ? ' has-danger' : '' }}">
                                <label class="mb-3" style="color:white; font-weight:bold">Employee role</label>
                                <select name="roles" class="form-control" >
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id}}" style="color:black" {{$user->role}} selected >
                                            {{ $role->role_name }}
                                        </option>
                                    @endforeach
                                </select>
                                @include('alerts.feedback', ['field' => 'role'])
                            </div>
                            @endif
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">Save</button>
                    </div>
                </form>
            </div>

            <div class="card col-md-5 ml-5 my-5">
                <div class="card-header">
                    <h5 class="title pb-3 pt-3" style="border-bottom:solid 1px white">Password Edit</h5>
                </div>
                <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                    <div class="card-body col-md-10 offset-1">
                        @csrf
                        @method('put')

                        @include('alerts.success', ['key' => 'password_status'])

                        <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                            <label class="mb-3" style="color:white; font-weight:bold">Current Password</label>
                            <input type="password" name="old_password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="Current Password" value="" required>
                            @include('alerts.feedback', ['field' => 'old_password'])
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label class="mb-3" style="color:white; font-weight:bold">New Password</label>
                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="New Password" value="" required>
                            @include('alerts.feedback', ['field' => 'password'])
                        </div>
                        <div class="form-group">
                            <label class="mb-3" style="color:white; font-weight:bold">Confirm New Password</label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm New Password" value="" required>
                        </div>
                    </div>
                    <div class="card-footer my-5">
                        <button type="submit" class="btn btn-fill btn-primary">Change password</button>
                    </div>
                </form>
            </div>
       
       
    </div>
@endsection
