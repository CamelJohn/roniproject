@extends('layouts.app')

@section('content')
    <div class="header py-7 py-lg-8">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6">
                        <h1 class="text-white"><i class="mr-3 tim-icons icon-send text-warning" style="font-size: 1em"></i>Unificient</h1>
                        <p class="text-lead text-light">
                            Use Unificient app to manage your oragnizations meetings/projects/overall-performance.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
