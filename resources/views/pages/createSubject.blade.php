@extends('layouts.app', ['page' => __('createMeeting'), 'pageSlug' => 'createMeeting'])

@section('content')
<div class="container my-5 ml-5 col-md-12" style="margin-left:20em !important; margin-top: 10em !important; font-family: 'Lobster', cursive;">
<div class="col-md-8 offest-2">
   @if($errors->any())
        <div class="alert alert-danger">
          <ul class = "list group" >
            @foreach($errors->all() as $error)
            <li class="list-group-item text-danger">
              {{$error}}
            </li>
            @endforeach
          </ul>
        </div>
    @endif
   </div>
  <div class="card card-header" style="width:42%;" >
    <h1 class="ml-5 my-2"> Create a new subject</h1>
  </div>
 
  <form action="{{route('storeSubject')}}" method="POST">
    @csrf

    <div class="card col-md-5 mr-1">
      <div class="card-body remove" id="subjects">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
            <label for="title">Subject title</label>
              <input type="text" class="form-control"  name="title">
            </div>
            <div class="col-md-5">
            <label for="subject_start">Subject starts at</label>
              <input type="time" name="subject_start" class="my-3.5 form-control" >
            </div>
            <div class="col-md-5">
              <input type="text" name="meeting_id" class="my-3.5 form-control" value="{{ $meeting->id}}" hidden>
            </div>
            <div class="col-md-5">
              <input type="time" name="meeting_start" class="my-3.5 form-control" value="{{ $meeting->meeting_start}}" hidden>
            </div>
            <div class="col-md-5">
              <input type="time" name="meeting_end" class="my-3.5 form-control" value="{{ $meeting->meeting_end}}" hidden>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card card-footer col-md-5">
        <div class="row">
        <div class="col-md-2 form-group">
            <button type="submit" class = "btn btn-success" style="font-family: 'Lobster', cursive;"> create </button>
        </div>
    <div class="col-md-2 offset-3 ">
    @if($numOfSubjects > 0)
      <a href="{{ route('showMeeting',$meeting->id)}}" class="btn btn-warning"> Done !</a>
    @endif
    </div>
        </div>
    </div>
  </form>
  
</div>

<ul>
  @foreach($errors->all() as $error)
    <p>{{$error}}</p>
  @endforeach
</ul>
@endsection