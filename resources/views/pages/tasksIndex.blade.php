@extends('layouts.app', ['page' => __('tasksIndex'), 'pageSlug' => 'tasksIndex'])

@section('content')

    <div class="card card-default" style="font-family: 'Lobster', cursive;">
        <div class="card card-header">
            <div class="row">
            @if($farsh)
            <h1 class="ml-5 my-5" ><span style="text-decoration:underline">@if(Auth::user()->role == 'employee') All {{Auth::user()->name}}'s' tasks @elseif(Auth::user()->role == 'CEO') All organization tasks @else All tasks {{Auth::user()->name}} assigned @endif  <i class="tim-icons icon-notes text-info" ></i></span><p class="text-muted my-2" style="font-size: 0.6em">viewing tasks: <span class="text-info">{{Auth::user()->name}}</span></p></h1>
            @else
            <h1 class="ml-5 my-5" ><span  style="text-decoration:underline">All <i class="tim-icons icon-trash-simple text-warning" ></i> tasks </span><p class="text-muted my-3" style="font-size:0.7em !important">viewing trashed tasks: <span class="text-info">{{Auth::user()->name}}</span></p></h1>
            @endif
            </div>
        </div>
        <div class="card card-body">
        @if($tasks->count()>0)
<table class="table">
        <tr>
            <th> Title </th>
            <th> Employee Name </th>
            <th> Assigned by </th> 
            <th> Related Meeting </th> 
            <th> Begin Date</th>
            <th> Due Date</th>
            <th> Status</th>
            <th> Message</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        @foreach($tasks as $task)
        <tr>
             <td><a href="#">{{$task->title}}</a></td>
            <td>
                @foreach($users as $user)
                    @if($user->id == $task->task_ordered)
                        {{$user->name}}
                    @endif
                @endforeach
            </td>
             <td>
                @foreach($users as $user)
                    @if($user->id == $task->user_id)
                        {{$user->name}}
                    @endif
                @endforeach
            </td>
             <td style="font-family: Arial;">
                @foreach($meetings as $meeting)
                    @if($meeting->id == $task->meeting_id)
                        <a class="btn btn-sm btn-success" href="{{ route('showMeeting', $meeting->id)}}">{{$meeting->meeting_title}}</a>
                    @endif
                @endforeach
            </td>
             <td >{{$task->task_start}}</td>
             <td>{{$task->task_end}}</td>
             <td>
             @if($task->status == 0)
                @if($atuh_user->id == $task->task_ordered )
                    <a class="text-danger" href="{{ route('taskStatus', $task->id)}}"> in progress</a>
                @else   
                    <p class="text-warning"> in progress </p>
                @endif
            @else      
                <a style="color:limegreen;" href="#"><i>done</i></a>
            @endif
            </td>
            <td>
                @if($task->status == 1)
                    @if($task->lateness == 1)
                        @if($atuh_user->id == $task->task_ordered )
                         <a class="text-danger" href="#">{{$task->message}} </a> 
                         @else
                            <p class="text-warning"><span class="text-danger" style="font-size:2em; text-decoration:underline !important">{{$task->message}}</span></p>
                         @endif
                    @endif
                @endif
             </td>
             <td></td>
             @cannot('employee')
             @if($task->trashed())
            <td>
                @if($task->user_id == $atuh_user->id || $atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager')
                <form action="{{ route('restore', $task->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                    <button type="submit" class="btn btn-info btn-sm">Restore</button>
                </form>
                @endif
            </td>
            @else
            <td>
                @if($task->status == false)
                    @if($task->user_id == $atuh_user->id || $atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager')
                        <a href="{{ route('editTask', $task->id)}}" class="btn btn-primary btn-sm"><i class="tim-icons icon-pencil"></i></a>
                    @endif
                @endif
            </td>
            @endif
            @if($task->status == false)
                @if($task->user_id == $atuh_user->id || $atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager')
                <td>
                <form action="{{ route('deleteTask', $task->id)}}" method="POST">
                    @csrf

                    @method('DELETE')
                    
                    <button type="submit" class="btn btn-warning btn-sm"> {{$task->trashed() ? 'Delete' : 'Trash'}}</button>
                </form>
                </td>
                @endif
            @endif
            @endcannot
        </tr>
        @endforeach
    </table>
    @else
    <h1 class="text-center">No Tasks yet</h1>
@endif
        </div>
    </div>
         
@endsection