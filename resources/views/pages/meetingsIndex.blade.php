@extends('layouts.app', ['page' => __('All Meetings'), 'pageSlug' => 'meetingsIndex'])

@section('content')
    
    @cannot('employee')
        @if(!$farsh)
        <div class="d-flex justify-content-end mb-2">
            <a href="{{ route('createMeeting') }} " class="btn btn-success btn-sm">Create a New meeting</a>
        </div>
        @endif
    @endcannot
<div class="card card-default col-md-8 offset-2 my-5" style="font-family: 'Lobster', cursive;">
    <div class="card card-header mb-0 my-4 ml-5">
    @if($farsh)
    <h1 class="mb-2"  style="text-decoration:underline">Trashed Meetings <i class="tim-icons icon-trash-simple text-warning" ></i></h1>
    <p class="text-muted">viewing trashed meetings: <span class="text-info">{{Auth::user()->name}}</span></p>
    @else
    <h1  style="text-decoration:underline">All Meetings</h1>
    <p class="text-muted">viewing meetings: <span class="text-info">{{Auth::user()->name}}</span></p>
    @endif
    </div>
    
    @if($meetings->count() > 0)
    <div class="card card-body">
    <table class="table">
    
    <tr>
        <th> Meeting Title </th>
        <th> Oragnizer </th>
        <th> # of Invited </th> 
        <th> Meeting date </th>
        <th> Meeting status </th> 
    </tr>
    @foreach($meetings as $meeting)
    <tr>
    <td>
        <a class="btn btn-sm btn-success" href="{{ route('showMeeting', $meeting->id)}}"> {{ $meeting->meeting_title}}</a>
        </td>
        <td>
            @foreach($inviter as $user)
                    @if($user->id == $meeting->user_id)
                            {{$user->name}}
                    @endif
            @endforeach
        </td>
        <td>
            <a href="#">{{ $meeting->users->count() }}</a>
        </td>
        <td>
            {{$meeting->meeting_date}}
        </td>
        @cannot('employee')
        @if($meeting->trashed())
        <td>
            @if($meeting->user_id == $atuh_user->id || $atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager')
            <form action="{{ route('restoreMeeting', $meeting->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                <button type="submit" class="btn btn-info btn-sm">Restore</button>
            </form>
            @endif
        </td>
        
        @else
            <td>
                @if($meeting->meeting_status == false)
                    <a style="color:limegreen;"> Open</a>
                @else
                    <p class="text-warning">Close</p>
                @endif
            </td>
            @if($meeting->meeting_status == false)
            <td>
                @if($meeting->user_id == $atuh_user->id || $atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager')
                    <a href="{{ route('editMeeting', $meeting->id)}}" class="btn btn-primary btn-sm"><i class="tim-icons icon-pencil"></i></a>
                @endif
            </td>
            @endif
        @endif
        @if($meeting->meeting_status == false)
            @if($meeting->user_id == $atuh_user->id || $atuh_user->role == 'CEO' || $atuh_user->role == 'executive_manager')
                <td>
                    <form action="{{ route('deleteMeeting', $meeting->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-warning btn-sm"> {{$meeting->trashed() ? 'Delete' : 'Trash' }}</button>
                    </form>
                </td>
            
            @endif
        @endif
        @endcannot
    </tr>
    @endforeach

</table>

@else 
   <h3 class="text-center"> No meetings yet</h3>
@endif
    </div>
</div>
@endsection