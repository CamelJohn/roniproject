@extends('layouts.app', ['page' => __('Show Meeting'), 'pageSlug' => 'meetingsIndex'])

@section('content')

<div class="container my-5 ml-4" style="font-family: 'Lobster', cursive;">
<div class="card card-header col-md-11 ml-1" style="width:86% !important" >
<h1 class="ml-2 my-2">Meeting title :  <i class="text-warning"> {{$meeting->meeting_title}}</i></h1>
<p class="ml-2 text-muted my-2">Meeting organizer: <span style="color:white">{{$user->name}}</span></p>
</div>

    <div class="row ml-1">

    <div class="card card-default col-md-3 mr-2">
    <div class="card card-header mb-0" style="border-bottom:solid white 1px; padding:1em">
    Employees 
    </div>
    <div class="card card-body">
       <table>
           <thead>
               <tr style="color:white; font-weight:bold" >
                   <th>Name</th>
                   <th>Attended</th>
               </tr>
           </thead>
           <tbody>
           @foreach($meeting->users as $user1)
            <tr style="color:white;">
                <td>{{ $user1->name}}</td>

                @if($user1->pivot->attended == 0 && $connected == $user->id)
                    @if($connected == $meeting->user_id)
                        <td>
                        @if($meeting->meeting_status == false)
                        <a href="{{route('attended',[$user1->id, $meeting->id])}}"><i class="tim-icons icon-pencil text-success mr-2 my-3"></i></a>
                        @endif
                        present ?</td>
                            @else
                            <td>Mark as present</td>
                    @endif
                    @else
                        @if($connected == $meeting->user_id)
                            <td><a href="#" style="color:limegreen;"><i>present</i></a></td>
                            @else
                                <td>present</td>
                        @endif
                @endif 
            </tr>
            @endforeach
           </tbody>
       </table>
    </div>
    </div>
     <div class="card card-default col-md-4 mr-2">
    <div class="card card-header mb-0" style="border-bottom:solid white 1px; padding:1em">
    Subjects
    </div>
    <div class="card card-body">
        <table>
            <thead>
                <tr style="color:white">
                    <th>Time</th>
                    <th>Title</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            @foreach($subjects as $subject)
            <tr >
                <td>{{ $subject->subject_start}}</td>
                @if($connected == $meeting->user_id)
                <td  style="color:white;">
                @if($meeting->meeting_status == false)
                <a href="{{route('editsubject', $subject->id)}}"><i class="tim-icons icon-pencil text-success mr-2 my-3"></i></a>
                @endif
                 {{ $subject->title}}</td>
                @else
                <td  style="color:white;"><a href="#">{{ $subject->title}}</a></td>
                @endif
                <td>
                    @if($subject->status == 0)
                        @if($connected == $meeting->user_id )
                            @if($meeting->meeting_status == false)
                            <a href="{{ route('subjectStatus', $subject->id)}}">in progress</i></a>
                        @else   
                            <a href="#">in progress  <i class="ml-1 text-info"></i></a>
                            @endif
                        @endif
                    @else
                        @if($connected == $meeting->user_id )
                            @if($meeting->meeting_status == false)
                                <a style="color:limegreen;" href="{{ route('subjectStatus', $subject->id)}}"><i>done</i></a>
                        @else   
                        <a style="color:limegreen;" href="#"><i>done</i></a>
                            @endif
                        @endif
                    @endif
                </td>
            </tr>
            
        @endforeach
            </tbody>
        </table>
    </div>
    </div>
    
    <div class="card card-default col-md-3">
    <div class="card card-header mb-0" style="border-bottom:solid white 1px; padding:1em">
    Info
    </div>
    <div class="card card-body">
                <p>
                @foreach($users as $user)
                        @if($user->id == $meeting->user_id)
                        Organizer: <a href="#">{{$user->name}}</a>
                        @endif
                @endforeach
            </p>
            <p>Begin: <a href="#">{{$meeting->meeting_start}}</a></p>
            <p>End: <a href="#">{{$meeting->meeting_end}}</a></p>
            <p>Duration (minutes): <a href="#">{{$length}}</a></p>
            
            <p>Number Attending: <a href="#">{{ $attendance->count() }}</a></p>
        </div>
    </div>
    </div>
    
    <div class="card card-header col-md-11 ml-1" style="width:86%">
    <div class="row">
    <div class="col-md-2">
        <a style="font-family: Arial;" href="{{route('meetingsIndex')}}" class="btn btn-sm btn-primary">Back</a>
    </div>
    <div class="col-md-2">
        <a style="font-family: Arial;" href="{{ route('createTask', $meeting->id)}}" class="btn btn-sm btn-success">add task</a>
    </div>
    @if($meeting->meeting_status == false)
    <div class="col-md-2">
        <a style="font-family: Arial;" href="{{ route('closeMeeting', $meeting->id)}}" class="btn btn-sm btn-warning">Meeting Ajurned</a>
    </div>
    <div class="col-md-2">
        <a style="font-family: Arial;" href="{{ route('createSubject', $meeting->id)}}" class="btn btn-sm btn-success">add new subject</a>
    </div>
    @endif
    </div>
    
</div>
</div>

@endsection