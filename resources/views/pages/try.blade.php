@extends('layouts.app', ['page' => __('dashboard'), 'pageSlug' => 'dashboard'])

@section('content')
<div class="container">
<div class="row">
<div class="card card-defautl">
    <div style="font-family: 'Lobster', cursive;"  class="card card-header">
    <h3 class="ml-4 my-2 text-muted"><span class="text-success">{{Auth::user()->name}}'s</span> dashboard</h3>
    <p class="ml-4 mb-0 text-muted">Role : <span class="text-warning">{{Auth::user()->role}}</span></p>
    </div>
</div>
</div>
    <div class="row">
        <div class="col-md-3 ">
            <div class="card crad-default">
                <div class="card card-header mb-0">
                    <h4 class="text-white" style="font-family: 'Lobster', cursive;">Total <i class="tim-icons icon-notes mr-2 ml-1 text-warning"></i>Tasks assigned</h4>
                    @if(Auth::user()->role == 'vp_coordinator')
                        <p class="ml-4 mb-0 text-muted">Assigend by <span class="text-success">{{Auth::user()->name}}</span></p>
                    @endif
                </div>
                <div class="card card-body text-center text-white" style="height:5em">
                    <h1 style="font-weight:bold">{{$countTasks}}</h1>
                </div>
            </div>
        </div>
       @if(Auth::user()->role == 'CEO' || Auth::user()->role == 'executive_manager')
       <div class="col-md-3 ">
            <div class="card crad-default">
                <div class="card card-header mb-0">
                    <h4 class="text-white" style="font-family: 'Lobster', cursive;"><i class="tim-icons icon-single-02 mr-1 text-info"></i>Total of employees</h4>
                </div>
                <div class="card card-body text-center text-white" style="height:5em">
                    <h1 style="font-weight:bold">{{$allOrgUsersCount}}</h1>
                </div>
            </div>
        </div>
       @endif
        <div class="col-md-3">
                <div class="card crad-default">
                        <div class="card card-header mb-0">
                            <h4 class="text-white" style="font-family: 'Lobster', cursive;"><i class="fab fa-laravel mr-1 ml-1 text-warning"></i> Tasks per employee</h4>
                        </div>
                        @if($countTasks == 0)
                            <h1 style="font-family: 'Lobster', cursive;" class="text-center">No Tasks late yet</h1>
                    @else
                    <div class="card card-body text-center text-white" style="height:5em">
                            <h1 style="font-weight:bold">{{number_format($avg,2)}}</h1>
                        </div>
                    @endif
                </div>
            </div>    
            <div class="col-md-3">
                <div class="card card-default">
                    <div class="card card-header mb-0" style="font-family: 'Lobster', cursive;">
                        <h4>Avg Task completion time<h4>
                    </div>
                    @if($countTasks == 0)
                            <h1 style="font-family: 'Lobster', cursive;" class="text-center">No Tasks completed yet</h1>
                    @else
                    <div class="card card-body text-center text-white mb-0" style="height:6em">
                                <h1 style="font-weight:bold">{{number_format(($taskTimes)/60, 2)}} <span style="font-size:10px">hrs</span></h1>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">   
            <div class="col-md-3 offset-1 mr-3 card card-default">
                <div class="card card-header" style="font-family: 'Lobster', cursive;">
                    <h4>Task <span class="text-success">completion</span> %</h4>
                </div>
                <div class="card card-body mb-0"style="height:10em !important">
                    {!! $usersChart2->container() !!}
                </div>
            </div>
            <div class="col-md-3  card card-default">
                <div class="card card-header" style="font-family: 'Lobster', cursive;">
                    <h4>Task <span class="text-warning">lateness</span> %</h4>
                </div>
                <div class="card card-body mb-0"style="height:10em !important">
                    {!! $usersChart5->container() !!}
                </div>
            </div>   
            @if(Auth::user()->role == 'CEO' || Auth::user()->role == 'executive_manager')
            <div class="col-md-4 ml-3 card card-default">
                <div class="card card-header mb-0" style="font-family: 'Lobster', cursive;">
                    <h4>Role precentages<h4>
                    <p class="text-muted" style="font-size:0.8em !important">count of roles in company by type</p>
                </div>
                <div class="card card-body mb-0"style="height:10em !important">
                    {!! $usersChart->container() !!}
                </div>
            </div>   
            @endif
    </div>    
@if(Auth::user()->role != 'employee')
    <div class="row ml-3 mb-0" style="width:99%;">
            <div class="card card-default">
                <div class="card card-header" style="font-family: 'Lobster', cursive">
                <h4>Tasks per month, <span class="text-warning">lateness</span> & <span class="text-success">completion</span> ratio<h4>
                @if(Auth::user()->role == 'vp_coordinator')
                        <p class="ml-4 mb-0 text-muted">Assigend by <span class="text-success">{{Auth::user()->name}}</span></p>
                @endif
                </div>
                <div class="card card-body"style="height:10em !important">
                @if($countTasks == 0)
                            <h1 style="font-family: 'Lobster', cursive;" class="text-center">No tasks assigned by you yet</h1>
                @else
                    {!! $usersChart1->container() !!}
                @endif
                </div>
            </div>
    </div>

    <div class="row ml-3 mb-0 my-0" style="width:99%;">
            <div class="card card-default">
                <div class="card card-header" style="font-family: 'Lobster', cursive;">
                    <h4>Tasks <span class="text-info">assigned</span> per employee, <span class="text-warning">lateness</span> & <span class="text-success">completion</span> ratio<h4>
                    @if(Auth::user()->role == 'vp_coordinator')
                        <p class="ml-4 mb-0 text-muted">Assigend by <span class="text-success">{{Auth::user()->name}}</span></p>
                    @endif
                </div>
                
                <div class="card card-body mb-0"style="height:15em !important">
                    @if($countTasks == 0)
                            <h1 style="font-family: 'Lobster', cursive;" class="text-center">No tasks assigned by you yet</h1>
                    @else
                        {!! $usersChart3->container() !!}
                    @endif
                </div>
            </div>
    </div>
    <div class="row">
    <div class="col-md-3 ml-3">
            <div class="card crad-default">
                <div class="card card-header mb-0">
                    <h4 class="text-white" style="font-family: 'Lobster', cursive;">Total  <i class="tim-icons icon-calendar-60 text-warning"></i> Meetings Attended</h4>
                    @if(Auth::user()->role == 'vp_coordinator')
                        <p class="ml-4 mb-0 text-muted">Meetings created by <span class="text-success">{{Auth::user()->name}}</span></p>
                    @endif
                </div>
                <div class="card card-body text-center text-white" style="height:5em">
                    <h1 style="font-weight:bold">{{$attended}}</h1>

                </div>
            </div>
        </div>
    <div class="col-md-3 ml-3">
            <div class="card crad-default">
                <div class="card card-header mb-0">
                    <h4 class="text-white" style="font-family: 'Lobster', cursive;"> <i class="tim-icons icon-calendar-60 text-warning"></i> @if(Auth::user()->role == 'vp_coordinator') Meetings ordered @elseif(Auth::user()->role == 'CEO' ||Auth::user()->role == 'executive_manager') All Organization Meetings @else Meetings invited to @endif</h4>
                    @if(Auth::user()->role == 'vp_coordinator')
                        <p class="ml-4 mb-0 text-muted">Meetings created by <span class="text-success">{{Auth::user()->name}}</span></p>
                    @endif
                </div>
                <div class="card card-body text-center text-white" style="height:5em">
                    @if(Auth::user()->role == 'vp_coordinator')
                        <h1 style="font-weight:bold">{{$countMeetingOrganizer}}</h1>
                    @elseif(Auth::user()->role == 'CEO' || Auth::user()->role == 'executive_manager')
                        <h1 style="font-weight:bold">{{$orgMeetingCount}}</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif
    

{!! $usersChart->script() !!}
{!! $usersChart1->script() !!}
{!! $usersChart2->script() !!}
@if(Auth::user()->role != 'employee')
{!! $usersChart3->script() !!}
@endif
{!! $usersChart5->script() !!}

@endsection 