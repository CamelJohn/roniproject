@extends('layouts.app', ['page' => __('createMeeting'), 'pageSlug' => 'createMeeting'])

@section('content')

    <div class="container my-5 ml-5 col-md-11" @isset($meeting) ? style="font-family: 'Lobster', cursive; left: 16em; top:7em" @else style="font-family: 'Lobster', cursive;" @endif>
   <div class="col-md-8 offest-2">
   @if($errors->any())
        <div class="alert alert-danger">
          <ul class = "list group" >
            @foreach($errors->all() as $error)
            <li class="list-group-item text-danger">
              {{$error}}
            </li>
            @endforeach
          </ul>
        </div>
    @endif
   </div>
    <div class="card card-header" @isset($meeting) ? style="width:59.5%" @else style="width:59.5%;" @endif >
    <h1 class="ml-5 my-2">{{isset($meeting) ? 'Edit this Meeting': 'Create New Meeting'}}</h1>
    </div>
 
     <form action="{{isset($meeting) ? route('updateMeeting',$meeting->id) : route('storeMeeting') }}" method="POST">
    @csrf

    @if(isset($meeting))
        @method('PUT')
    @endif
    
<div class="accordion ml-3" id="accordionExample">
<div class="row">
<div class="card col-md-4 mr-1">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0" style="padding-bottom:1em">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <a href="#" >{{isset($meeting) ? 'Edit meeting date': 'Set meeting date'}}</a>
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body ml-2">
          <div class="row">
          <div class="form-group">
            <label for="title">Meeting Title</label>
            <input type="text" class="form-control my-2" name="meeting_title" value="{{isset($meeting) ? $meeting->meeting_title : '' }}">
            <span class="invalid-feedback" role="alert">$errors->get('meeting_title')</span>
          </div>            
            <div class="form-group">
            <div class="row">
            <div class="col-md-5 ">
            <label for="">from</label>
            <input type="time" class="form-control my-2" name="meeting_start" value="{{isset($meeting) ? $meeting->meeting_start : '' }}"   >
            </div>
            <div class="col-md-5 offset-1 ">
            <label for="">to</label>
            <input type="time" class="form-control my-2" name="meeting_end" value="{{isset($meeting) ? $meeting->meeting_end : '' }}"    >
            </div>
            <div class="col-md-5">
            <label for="">date</label>
            <input type="date" class="form-control my-2" name="meeting_date" value="{{isset($meeting) ? $meeting->meeting_date : '' }}" id='date'  >
            </div>
            </div>
          </div>
          </div>
      </div>
    </div>
  </div>

  <div class="card card col-md-3">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <a href="#">{{isset($meeting) ? 'Edit users in Meeting': 'Add Users to meeting'}}</a>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
         <select name="users[]" id="users" class="form-control" multiple>
                  @foreach($users as $user)
                      <option value="{{ $user->id}}"
                        @if(isset($meeting)) {{$meeting->user_id}} selected
                        @endif>
                          {{ $user->name }}
                      </option>
                  @endforeach
         </select>
      </div>
    </div>
  </div>
</div>
</div>

<div class="form-group">
    <button type="submit" class = "btn btn-success">
        {{isset($meeting) ? 'Edit': 'Next'}}    
    </button>
    <a style="font-family: Arial;" href="{{route('meetingsIndex')}}" class="btn btn-primary ml-5">Back</a>
</div>

</form>
    </div>



 @endsection




@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script> 
         
        flatpickr('#date', {enableTime:false})
    </script>

    <script type="text/javascript">
            var count = 1;
            var names = ['one','two','three','four','five'];
            var title ='';
            var count2 =6;
         
            $('#addRow').on('click', function () {
              if(count<=5){
                title=names[count-1];
                addRow();
                count +=1;
                count2 +=1;
              }
            });


            function addRow(){
              var forminput = 
              '<div class="form-group" id="target'+count+'">'+
                '<div class="row">'+
                  '<div class="col-md-6">'+
                    '<label for="title">Subject '+title+'</label>'+
                    '<input type="text" class="form-control" name="'+count+'">'+
                  '</div>'+
                  '<div class="col-md-6">'+
                  '<label for="'+count2+'">Start at</label>'+
                  '<input type="time" class="form-control" name="'+count2+'">'+
                  '</div>'+
                '</div>'+
              '</div>';
              
              $('#subjects').append(forminput);
            };
            
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection