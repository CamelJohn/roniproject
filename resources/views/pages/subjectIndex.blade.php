@extends('layouts.app' ,['page' => __('subjectIndex'), 'pageSlug' => 'subjectIndex'])

@section('content')
<div class="d-flex justify-content-end mb-2 m-2">
    <a href=" " class="btn btn-success btn-sm">Add New Meeting Task</a>
</div>

 

 <table class="table">
        <tr>
            <th> Meeting Number </th>
            <th> Meeting Task Title </th>
            <th> Task Started At </th>
             <th> Task Status </th> 
            <th> </th>
            <th> </th>
            
        </tr>
       @foreach($subjects as $subject)
        <tr>
            <td>
                {{ $subject->meeting_id}}
            </td>
            <td>
               {{ $subject->title }}
            </td>
            
            <td>
                {{ $subject->subject_start}}
            </td>
            <td>
            {{ $subject->status}}

            </td>
            <td>
            </td>
            <td>
                <a href="{{route('pages.createMeeting', $subject->id)}}" class="btn btn-info btn-sm">Edit</a>
            </td>
            <td>
                <a href="" class="btn btn-danger btn-sm">Delete</a>
            </td>

        </tr>
        @endforeach
    </table>
 
@endsection