@extends('layouts.app', ['page' => __('createTask'), 'pageSlug' => 'createTask'])

@section('content')

<div class="container" style="font-family: 'Lobster', cursive;">
<div class="col-md-8 offest-2">
   @if($errors->any())
        <div class="alert alert-danger">
          <ul class = "list group" >
            @foreach($errors->all() as $error)
            <li class="list-group-item text-danger">
              {{$error}}
            </li>
            @endforeach
          </ul>
        </div>
    @endif
   </div>
    <div class="card card-default col-md-6 offset-2 my-5">
        <div class="card card-header">
            <h3>{{isset($tasks) ? 'Edit this Task': 'Create New Task'}}</h3>
        </div>
        <div class="card card-body">
        <form action="{{isset($tasks) ? route('updateTask',$tasks->id) : route('storeTask',$meeting->id) }}" method="POST">
    @csrf

    @if(isset($tasks))
        @method('PUT')
    @endif
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" value="{{isset($tasks) ? $tasks->title : '' }}" placeholder="Task Title">
    </div>

    <div class="form-group">
        <label for="">From</label>
        <input type="text" class="form-control" name="task_start" value="{{isset($tasks) ? $tasks->task_start : '' }}" id='start'>
    </div>
    <div class="form-group">
        <label for="">Until</label>
        <input type="text" class="form-control" name="task_end" value="{{isset($tasks) ? $tasks->task_end : '' }}" id='end'>
    </div>
    
    @if(isset($tasks))
    <div class="form-group">
        <label for="meetings">Assign to meeting</label>
        <select name="meetings" id="meetingss" class="form-control" style="background-color: #222a42">
                    @foreach($meetings as $meeting)
                        <option value="{{ $meeting->id}} ">
                            {{ $meeting->meeting_title}}
                        </option>
                    @endforeach
            </select>
        <input type="text" name="users" value="{{$tasks->task_ordered}}" hidden>   
    </div>
    @endif
    @if(!isset($tasks))
    <div class="form-group">
    <label for="users">Assign to User</label>
    <select name="users" id="meetingss" class="form-control" style="background-color: #222a42">
                  @foreach($meeting_users as $meeting_user)
                      <option value="{{ $meeting_user->id}} ">
                          {{ $meeting_user->name}}
                      </option>
                  @endforeach
         </select>
    </div>
    @endif
    <!-- <div class="card card-header col-md-11 ml-1" style="width:50%"> -->
    <div class="row">
        <div class="col-md-1">
            <a href="{{route('tasksIndex')}}" class="btn btn-sm btn-warning">Back</a>
        </div>
        <div class="col-md-1 offset-2">
            <div class="form-group">
            <button type="submit" class="btn btn-sm btn-success">
                {{isset($tasks) ? 'Edit': 'Assing Task'}}
            </button>
            </div>
        </div>
    </div>
   
    
<!-- </div> -->

    
</form>
        </div>
    </div>
</div>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

 
@endsection


@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script> 
        flatpickr('#start', {enableTime:true})
        flatpickr('#end', {enableTime:true})
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection