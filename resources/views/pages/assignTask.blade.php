@extends('layouts.app', ['page' => __('createMeeting'), 'pageSlug' => 'createMeeting'])

@section('content')

     {{isset($meeting) ? 'Edit this Meeting': 'Create New Meeting'}}      
 
     <form action="{{isset($meeting) ? route('pages.updateMeeting',$meeting->id) : route('pages.storeMeeting') }}" method="POST">
    @csrf

    @if(isset($meeting))
        @method('PUT')
    @endif
    
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <a href="#">{{isset($meeting) ? 'Edit this Meeting': 'Create New Meeting'}}</a>
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body">
          <div class="form-group">
            <label for="title">Meeting Title</label>
            <input type="text" class="form-control" name="meeting_title" value="{{isset($meeting) ? $meeting->meeting_title : '' }}">
          </div>

          <div class="form-group">
            <label for="">From</label>
            <input type="text" class="form-control" name="meeting_start" value="{{isset($meeting) ? $meeting->meeting_start : '' }}" id='start'>
          </div>

          <div class="form-group">
            <label for="">Until</label>
            <input type="text" class="form-control" name="meeting_end" value="{{isset($meeting) ? $meeting->meeting_end : '' }}" id='end'>
          </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <a href="#">{{isset($meeting) ? 'Edit Meeting Subjects': 'Add a New Subject'}}</a>
        </button>
      </h2>
    </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
          
          <div class="card-body remove" id="subjects">
          @if(!isset($meeting))
          <a href="#" id="addRow" class="btn btn-warning mb-4">add subject</a>
          @endif
          
          @if(isset($meeting))
         @foreach($subjects as $subject)
          <div class="form-group">
                <div class="row">
                  <div class="col-md-3">
                    <input type="text" class="form-control" value="{{$subject->title}}" name="{{$subject->id}}">
                  </div>
                  <div class="col-md-2">
                   <input type="time" name="subject_start" class="my-3.5 form-control" value="{{$subject->subject_start}}">
                  </div>
                  <div class="col-md-2">
                  </div>
                </div>
              </div>
              @endforeach
          @else
          @endif
          </div>
         
        </div>
    </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <a href="#">{{isset($meeting) ? 'Edit users in Meeting': 'Add Users to meeting'}}</a>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
         <select name="users[]" id="users" class="form-control" multiple>
                  @foreach($users as $user)
                      <option value="{{ $user->id}}"
                        @if(isset($meeting))
                          @if($meeting->user_id == $meeting->user_id)
                            selected
                          @endif
                        @endif>
                          {{ $user->name }}
                      </option>
                  @endforeach
         </select>
      </div>
    </div>
  </div>
</div>

 




<div class="form-group">
    <button type="submit" class = "btn btn-success">
        {{isset($meeting) ? 'Edit Meeting': 'Create Meeting'}}    
    </button>

</div>

</form>
 @endsection




@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script> 
        flatpickr('#start', {enableTime:true})
        flatpickr('#end', {enableTime:true})
    </script>

    <script type="text/javascript">
            var count = 1;
            var names = ['one','two','three','four','five'];
            var title ='';
            $('#addRow').on('click', function () {
              if(count<=5){
                title=names[count-1];
                addRow();
                count +=1;
              }
            });


            function addRow(){
              var forminput = 
              '<div class="form-group" id="target'+count+'">'+
                '<div class="row">'+
                  '<div class="col-md-3">'+
                    '<label for="title">Subject '+title+'</label>'+
                    '<input type="text" class="form-control" name="'+count+'">'+
                  '</div>'+
                  '<div class="col-md-2">'+
                   '<input type="time" name="subject_start" class="my-4 form-control">'+
                  '</div>'+
                  '<div class="col-md-2">'+
                   '</div>'+
                '</div>'+
              '</div>';
              
              $('#subjects').append(forminput);
            };


            $('#remove').on('click', function(){
              $('#target').remove();
            });
            
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection