@extends('layouts.app', ['page' => __('createMeeting'), 'pageSlug' => 'createMeeting'])

@section('content')
<div class="container my-5 ml-5 col-md-12" style="margin-left:20em !important; margin-top: 10em !important; font-family: 'Lobster', cursive;">
  <div class="card card-header" style="width:42%;" >
    <div class="ml-2 my-2 row">
    <h1> Edit Subject : </h1>
    <h1 class="text-warning ml-2">{{$subject->title}}</h1>
    </div>
  </div>
 
  <form action="{{route('updateSubject',$subject->id)}}" method="POST">
    @csrf

    @method('PUT')

    <div class="card col-md-5 mr-1">
      <div class="card-body remove" id="subjects">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
            <label for="title">Subject title</label>
              <input type="text" class="form-control" value="{{$subject->title}}" name="title">
            </div>
            <div class="col-md-5">
            <label for="subject_start">Subject starts at</label>
              <input type="time" name="subject_start" class="my-3.5 form-control" value="{{$subject->subject_start}}">
            </div>
            <div class="form-group col-md-6 my-3 ml-1">
              <label for="meetings">Assign to meeting</label>
                <select name="meetings" id="meetingss" class="form-control" style="background-color: #222a42">
                  @foreach($meetings as $meeting)
                    <option value="{{ $meeting->id}}" @if($subject->meeting_id == $meeting->id) {{$subject->meeting_id}} selected
                        @endif>
                        {{ $meeting->meeting_title}}
                    </option>
                  @endforeach
                </select> 
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="card card-footer col-md-5">
    <div class="form-group">
      <button type="submit" class = "btn btn-success"> Edit </button>
    </div>
    </div>
  </form>
  <div class="card card-footer col-md-5">
    <form method = 'post' action="{{ route('deleteSubject', $subject->id)}}">
      @csrf
      @method('DELETE')
      <div class = "form-group">
        <button type="submit" class = "btn btn-danger"> Delete </button>
      </div>
    </form>
  </div>
</div>
</div>

<ul>
  @foreach($errors->all() as $error)
    <li>{{$error}}</li>
  @endforeach
</ul>
@endsection