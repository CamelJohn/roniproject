<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-normal" style="text-align:center">www.Unificient.com</a>
        </div>
        <ul class="nav">
            <li @if ($pageSlug == 'dashboard') class="active" @endif>
                <a href="{{ route('pages.try') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#organization_users" aria-expanded="true">
                    <i class="fab fa-laravel" ></i>
                    <span class="nav-link-text" >Employee Management</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse" id="organization_users">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug == 'profile') class="active " @endif>
                            <a href="{{ route('profile.edit')  }}">
                                <i class="tim-icons icon-single-02"></i>
                                <p>My Profile</p>
                            </a>
                        </li>
                        <li @if ($pageSlug == 'users') class="active " @endif>
                            <a href="{{ route('user.index')  }}">
                                <i class="tim-icons icon-bullet-list-67"></i>
                                <p>Employee index</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#meetings" aria-expanded="true">
                <i class="tim-icons icon-calendar-60" ></i>
                    <span class="nav-link-text" >Meetings</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse" id="meetings">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug == 'meetingsIndex') class="active " @endif>
                            <a href="{{ route('meetingsIndex') }}">
                                <i class="fab fa-linode"></i>
                                <p>Meetings Index</p>
                            </a>
                        </li>
                        @if(Auth::user()->role != 'employee')
                        <li @if ($pageSlug == 'createMeeting') class="active " @endif>
                            <a href="{{ route('createMeeting') }}">
                                <i class="fab fa-mixcloud"></i>
                                <p>create Meeting</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </li>
            <li>
                        <li @if ($pageSlug == 'tasksIndex') class="active " @endif>
                            <a href="{{ route('tasksIndex') }}">
                            <i class="tim-icons icon-notes" ></i>
                                <p>Tasks Index</p>
                            </a>
                        </li>
            </li>
            <li>
                <a data-toggle="collapse" href="#trash" aria-expanded="true">
                    <i class="tim-icons icon-trash-simple" ></i>
                    <span class="nav-link-text" >Trash</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse" id="trash">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug == 'trashMeetings') class="active " @endif>
                            <a href="{{ route('trashedMeeting') }}">
                            <i class="tim-icons icon-calendar-60" ></i>
                                <p>Trashed meetings</p>
                            </a>
                        </li>
                        <li @if ($pageSlug == 'trashTasks') class="active " @endif>
                            <a href="trashedTask">
                            <i class="tim-icons icon-notes" ></i>
                                <p>Trashed tasks</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
