<footer class="footer">
    <div class="container-fluid">
 
        <div class="copyright">
            &copy; {{ now()->year }} made with <a href=""><i class="tim-icons icon-heart-2"></i></a> by
            <a href="" target="_blank"> Unificient</a> &amp;
            <a href="" target="_blank">Team Azrieli</a> for a better organization.
        </div>
    </div>
</footer>
