@extends('layouts.app',['page' => __(''), 'pageSlug' => 'profile'])

@section('content')
    <div class="row" style="margin-left:15em">
        <div class="col-md-7 mr-auto">
            <div class="card card-register card-white" style="width:65%;">
                <div class="card-header">
                    <img class="card-img" src="{{ asset('black') }}/img/card-primary.png" style="height:11em; margin-top: 4.3em; width: 130%" alt="Card image">
                    <h4 class="card-title" style="margin-left:10px; font-size:2.5em;">Change Password</h4>
                    
                </div>
                <form method="POST" action="{{ route('profile.update') }}" autocomplete="off">
                    @csrf

                    @method('PUT')

                    <div class="card-body">
                    <div class="col-md-11">
                    <p style="color:black;">You must change your initial password</p>
                    </div>
                        <div class="col-md-11">
                        <div class="input-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="tim-icons icon-lock-circle"></i>
                                </div>
                            </div>
                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="New Password" required>
                            @include('alerts.feedback', ['field' => 'password'])
                        </div>
                        </div>
                        <div class="col-md-11">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="tim-icons icon-lock-circle"></i>
                                </div>
                            </div>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm New Password" required>
                        </div>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-round btn-lg">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
