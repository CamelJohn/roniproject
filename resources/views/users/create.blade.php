@extends('layouts.app', ['page' => __('User Management'), 'pageSlug' => 'users'])

@section('content')
    <div class="container-fluid mt--7" style="font-family: 'Lobster', cursive;">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">User Management</h3>
                            </div>
                          
                        </div>
                    </div>
                   
                    <div class="card-body">
                        <form method="post" action="{{ route('userStore') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">User information</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">Name</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>
                                <div class="form-group{{ $errors->has('Phone') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">Phone</label>
                                    <input type="phone" name="phone" id="input-name" class="form-control form-control-alternative{{ $errors->has('Phone') ? ' is-invalid' : '' }}" placeholder="Phone" value="{{ old('Phone') }}" required autofocus>
                                    @include('alerts.feedback', ['field' => 'Phone'])
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-email">Email</label>
                                    <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{ old('email') }}" required>
                                    @include('alerts.feedback', ['field' => 'email'])
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-password">Password</label>
                                    <input type="password" name="password" id="input-password" class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" value="" required>
                                    @include('alerts.feedback', ['field' => 'password'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-password-confirmation">Confirm Password</label>
                                    <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control form-control-alternative" placeholder="Confirm Password" value="" required>
                                </div>

                                <div class="form-group">
                                    <label for="organization">How invited you?</label>
                                        <select name="organization" class="form-control" style="background-color: #222a42">
                                            @foreach($users as $user)
                                                <option value="{{ $user->organization_id}} ">
                                                    {{ $user->email}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'password'])
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
