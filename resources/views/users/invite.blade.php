@extends('layouts.app', ['page' => __('User Management'), 'pageSlug' => 'users'])

@section('content')
<div class="container" style="font-family: 'Lobster', cursive;">
    <div class="row justify-content-center my-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header"><h4 class="pb-3 my-3" style="text-align:center; font-weight:bold">Invite Employee</h4></div>

                <div class="card-body">
                    <form method="POST" action="{{url('send/email')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="subject" class="col-md-4 col-form-label text-md-right" style="color:white; font-weight:bold">Subject</label>

                            <div class="col-md-6">
                                <input id="subject" name="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" value="{{ old('subject') }}" 
                                required autocomplete="subject" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right" style="color:white; font-weight:bold">Email</label>

                            <div class="col-md-6">
                                <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" name="subject" value="{{ old('subject') }}" 
                                required autocomplete="email" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
 
                            <div class="col-md-6">
                                <textarea name="message" id="" cols="5" rows="3" hidden>
                                    {{URL::signedRoute('addUser')}}
                                    
                                    you are invited by {{Auth::user()->email}}
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-warning">Send Email</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
