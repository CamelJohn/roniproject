@extends('layouts.app', ['page' => __('User Management'), 'pageSlug' => 'users'])

@section('content')

    <div class="row" >
        <div class="ml-5 my-5">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h1 class="card-title my-3" style="font-family: 'Lobster', cursive;">All employees <i class="fab fa-laravel text-info"></i></h1>
                        </div>
                        @if(Auth::user()->role == 'CEO' || Auth::user()->role == 'executive_manager')
                        <div class="col-4 text-right">
                            <a href="{{ route('invite') }}" class="btn btn-sm btn-success">Invite user<i class="tim-icons icon-send ml-2"></i></a>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @include('alerts.success')

                    
                        <table class="table tablesorter" id="">
                            <thead class=" text-primary">
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Role</th>
                                <th scope="col">Creation Date</th>
                                <th scope="col"></th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr style="font-family: 'Lobster', cursive;">
                                        <td>{{ $user->name }}</td>
                                        <td>
                                            <a class="text-info" href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                        </td>
                                        <td>{{$user->phone}}</td>
                                        
                                        <td><div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    @if($user->role == 'CEO')
                                                    <p class="text-warning">{{$user->role}}</p>
                                                    @elseif($user->role == 'executive_manager')
                                                    <p class="text-success">{{$user->role}}</p>
                                                    @elseif($user->role == 'vp_coordinator')
                                                    <p class="text-primary">{{$user->role}}</p>
                                                    @elseif($user->role == 'employee')
                                                    <p style="color:white !important">{{$user->role}}</p>
                                                    @endif
                                            </a>
                                            </div>
                                        </td>
                                        <td>{{ $user->created_at->format('d-m-Y H:i') }}</td>                                      
                                       @if(Auth::user()->role == 'CEO' || Auth::user()->role == 'executive_manager')
                                        <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        @if (auth()->user()->id != $user->id)
                                                            <form action="{{ route('user.destroy', $user) }}" method="post">
                                                                @csrf
                                                                @method('delete')

                                                                <a class="dropdown-item" href="{{ route('user.edit', $user) }}">Edit</a>
                                                                <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                                                            Delete
                                                                </button>
                                                            </form>
                                                        @else
                                                            <a class="dropdown-item" href="{{ route('profile.edit') }}">Edit</a>
                                                        @endif
                                                    </div>
                                                </div>
                                        </td>
                                        @endif
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                   
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       
                    </nav>
                </div>
            </div>
        </div>
    </div>
  
@endsection
