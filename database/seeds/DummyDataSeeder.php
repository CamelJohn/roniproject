<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('users')->insert([
            'name' => 'Jonathan Atia',
            'email' => 'Jonathan23986@gmail.com',
            'role' => 'CEO',
            'organization_id' => 200,
            'email_verified_at' => now(),
            'password' => Hash::make('Avigail2017'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'name' => 'Noam Amron',
            'email' => 'noam0574@gmail.com',
            'role' => 'CEO',
            'organization_id' => 100,
            'email_verified_at' => now(),
            'password' => Hash::make('noam2019'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        foreach(range(1,20) as $index){
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'role' => 'employee',
                'email_verified_at' => now(),
                'password' => Hash::make('secret'),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
