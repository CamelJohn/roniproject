<?php

use Illuminate\Database\Seeder;

class UsersRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'role_name' => 'CEO',
            ],
            [
                'role_name' => 'executive_manager',
            ],
            [
                'role_name' => 'vp_coordinator',
            ],
            [
                'role_name' => 'employee',
            ],
        ]);
    }
}
