<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class]);
        $this->call([UsersRoleSeeder::class]);
        $this->call([DummyDataSeeder::class]);
        $this->call([EmployeesSeeder::class]);
    }
}
